module.exports = {
  // モード値を production に設定すると最適化された状態で、
  // development に設定するとソースマップ有効でJSファイルが出力される
  mode: "development",

  // メインのJS
  entry: ["@babel/polyfill", "./src/js/index.js"],
  // 出力ファイル
  output: {
    filename: "js/bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.js/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader"
          }
        ]
      }
    ]
  },
  resolve: {
    modules: [`${__dirname}/src/js`, "node_modules"],
    extensions: [".js"]
  }
};
