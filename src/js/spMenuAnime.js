import anime from "animejs/lib/anime.es";

export const menuOpen = document
  .querySelector("#hbg")
  .addEventListener("touchstart", () => {
    event.preventDefault();
    anime({
      targets: "#spMenu",
      top: 0,
      easing: "easeInOutQuad",
      duration: 500
    });
  });

export const menuClose = document
  .querySelector("#closeMenu")
  .addEventListener("touchstart", () => {
    event.preventDefault();
    anime({
      targets: "#spMenu",
      top: "-150vh",
      easing: "easeInOutQuad",
      duration: 500
    });
  });
