var gulp = require("gulp");
var sass = require("gulp-sass");
var sassGlob = require("gulp-sass-glob");
var plumber = require("gulp-plumber");
var rename = require("gulp-rename");
var minify = require("gulp-csso");
var browserSync = require("browser-sync");
var sourcemaps = require("gulp-sourcemaps");
var postcss = require("gulp-postcss");
var cssnext = require("postcss-cssnext");
var cleanCSS = require("gulp-clean-css");
var runSequence = require("run-sequence");
var del = require("del");
var autoprefixer = require("gulp-autoprefixer");

// imagemin
var newer = require("gulp-newer");
var imagemin = require("gulp-imagemin");
var imageminJpg = require("imagemin-jpeg-recompress");
var imageminPng = require("imagemin-pngquant");
var imageminGif = require("imagemin-gifsicle");
var svgmin = require("gulp-svgmin");
var mozjpeg = require("imagemin-mozjpeg");

// uglify
var uglify = require("gulp-uglify");

// webpack
var webpackStream = require("webpack-stream");
var webpack = require("webpack");
const webpackConfig = require("./webpack.config");

// const path = require("path");
// const distDir = path.resolve(process.cwd(), "dist");
// const userDir =
//   process.env[process.platform === "win32" ? "USERPROFILE" : "HOME"];
// // const sslDir = path.resolve(userDir, "etc", "ssl"); // ~/ect/ssl に相当

var paths = {
  src: "src/",
  dest: "assets/",

  scss: "src/scss/",
  css: "assets/css/"
};

var srcGlob = paths.src + "images/**/*.+(jpg|jpeg|png|gif)";
var srcSVGGlob = paths.src + "images/**/*.+(svg)";
var dstGlob = paths.dest + "images/";

gulp.task("bs", function() {
  browserSync({
    proxy: "vccw.test",
    https: false,
    port: 3300,
    // tunnel: true,
    // tunnel: "localdev",
    files: ["./assets/**/*.css", "./assets/**/*.js", "./**/*.php"]
  });
});

// CSS
gulp.task("scss", function() {
  var processors = [cssnext()];
  return gulp
    .src(paths.src + "scss/**/*.scss")
    .pipe(
      plumber({
        errorHandler: function(err) {
          console.log(err.messageFormatted);
          this.emit("end");
        }
      })
    )
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(
      sass({
        outputStyle: "expanded"
      })
    )
    .pipe(
      autoprefixer({
        browsers: ["last 2 version", "iOS >= 8.1", "Android >= 4.4"],
        cascade: false
      })
    )
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.dest + "css/"))
    .pipe(
      browserSync.reload({
        stream: true
      })
    );
});

// jpg,png,gif画像の圧縮タスク
gulp.task("imagemin", function() {
  return gulp
    .src(paths.src + "images/**/*")
    .pipe(newer(paths.dest))
    .pipe(
      imagemin([
        imageminPng(),
        imageminJpg(),
        imageminGif({
          interlaced: false,
          optimizationLevel: 3,
          colors: 180
        })
      ])
    )
    .pipe(gulp.dest(paths.dest + "images"));
});

// svg画像の圧縮タスク
gulp.task("svgmin", function() {
  gulp
    .src(srcSVGGlob)
    .pipe(newer(dstGlob))
    .pipe(svgmin())
    .pipe(gulp.dest(dstGlob));
});

gulp.task("bs-reload", function() {
  browserSync.reload();
});

gulp.task("watch", function() {
  gulp.watch(paths.src + "scss/**/**.scss", ["scss"]);
  gulp.watch("./**/*.+(php|html)", ["bs-reload"]);
  gulp.watch(paths.src + "js/*.js", ["bs-reload", "webpack"]);
  gulp.watch(paths.src + "images/**/*", ["imagemin", "svgmin"]);
});

// gulp.task('default', ['bs', 'prettify', 'scss', 'watch']);
gulp.task("default", ["bs", "watch"]);
gulp.task("optimg", ["imagemin", "svgmin"]);

//CSS圧縮
gulp.task("minify", function() {
  return gulp
    .src("assets/css/style_n.css")
    .pipe(cleanCSS())
    .pipe(gulp.dest(paths.dest + "css/"));
});

// js
gulp.task("uglify", function() {
  return gulp
    .src(paths.src + "js/**/*.js")
    .pipe(plumber())
    .pipe(uglify({ output: { comments: "some" } }))
    .pipe(
      rename({
        extname: ".min.js"
      })
    )
    .pipe(gulp.dest(paths.dest + "js/"));
});

// webpack task
gulp.task("webpack", function() {
  return webpackStream(webpackConfig, webpack)
    .on("error", function(e) {
      this.emit("end");
    })
    .pipe(gulp.dest(paths.dest));
});

// clean
gulp.task("clean", function() {
  del(["assets/**/*"]);
});

// 本番公開用
gulp.task("prod", function() {
  return runSequence("clean", "webpack", "scss", "optimg", "minify", "copy");
});

// fontコピー
gulp.task("copy", function() {
  return gulp
    .src(["src/webfonts/**"], { base: "src" })
    .pipe(gulp.dest("assets"));
});
