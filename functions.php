<?php

// CSS / JS のenqueueを行います
function my_enqueue_files() {
    // WordPress本体のjquery.jsを読み込まない
    wp_deregister_script('jquery');
    // jQueryの読み込み　(このあたりは適宜書き換えてください)
    wp_enqueue_script( 'jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js', "", "20170705", false );
    wp_enqueue_script( 'bundle', get_stylesheet_directory_uri() . '/assets/js/bundle.js', "", '1.0.0', true );

    // サイト共通のCSSの読み込み
    wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/assets/css/style.css', "", '20190513' );
}
add_action( 'wp_enqueue_scripts', 'my_enqueue_files' );


// 絵文字を咎めるやつ
function disable_emoji() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
add_action( 'init', 'disable_emoji' );


// visual editorのおせっかいを咎めるやつ
// オートフォーマット関連の無効化
// これは結構運用の都合もあるので、デフォルトでは無効にしておきます。
// 必要であればコメントを解除してください
add_action('init', function() {
    remove_filter('the_title', 'wptexturize');
    remove_filter('the_content', 'wptexturize');
    remove_filter('the_excerpt', 'wptexturize');
    remove_filter('the_title', 'wpautop');
    remove_filter('the_content', 'wpautop');
    remove_filter('the_excerpt', 'wpautop');
    remove_filter('the_editor_content', 'wp_richedit_pre');
});

add_filter('tiny_mce_before_init', function($init) {
    $init['wpautop'] = false;
    $init['apply_source_formatting'] = true;
    return $init;
});


// アーカイブページで簡単にタームのslagやnameが使用できるスニペットです。
function get_current_term(){
    $id;
    $tax_slug;

    if(is_category()){
        $tax_slug = "category";
        $id = get_query_var('cat');
    }else if(is_tag()){
        $tax_slug = "post_tag";
        $id = get_query_var('tag_id');
    }else if(is_tax()){
        $tax_slug = get_query_var('taxonomy');
        $term_slug = get_query_var('term');
        $term = get_term_by("slug",$term_slug,$tax_slug);
        $id = $term->term_id;
    }

    return get_term($id,$tax_slug);
}

// アイキャッチの有効化
add_theme_support('post-thumbnails');
// お好きなサイズを追加編集してください
// add_image_size('gallery_main', 460, 420, true);
//add_image_size('voice_square', 240, 260, true);
//add_image_size('banner', 490, 210, true);

function shortcode_url() {
    return get_bloginfo('url');
}
add_shortcode('url', 'shortcode_url');
/* 投稿内で [url] と記述する */


add_shortcode('template_url', 'shortcode_templateurl');
function shortcode_templateurl() {
    return get_bloginfo('template_url');
}
/* 投稿内で [template_url] と記述する */

// srcsetにショートコードを効かせるやつ
add_filter( 'wp_kses_allowed_html', 'my_wp_kses_allowed_html', 10, 2 );
function my_wp_kses_allowed_html( $tags, $context ) {
    $tags['img']['srcset'] = true;
    return $tags;
}

// ログイン画面の画像を変更するやつです　必要であれば適宜修正
function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/images/logo.png);
            padding-bottom: 10px;
            background-size: cover;
            width: 240px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
